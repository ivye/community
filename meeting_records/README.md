# Project Community Meeting

**NOTE**
1. This meeting introduction is for the project [community](/), not for the whole openEuler Community. 
2. Chinese is not supposed to provide for this README, as the contributors are from all of the world speaking many different languages. If you would like to provide a Chinese version, feel free to create a *README_cn.md* here.

### 1. Weekly meeting
- Time
    10:00-11:00 am, Tuesday, Beijing Time; 2:00-3:00 am, Tuesday, UTC.

- Channels

    IRC will be used to have the weekly meeting. If you consider other meetings like zoom, please propose to [mail list](community@openeuler.org) in advance. 

    The channel https://webchat.freenode.net/#openeuler-meeting is used.

- Document for meetings

    If there are some documents for the meeting, please `cp ./meeting_records/template_meetig ./meeting_records/yyyy-mm-dd-meeting` and put the documents there so that attendees can find them. 

### 2. Agenda for each meeting

* #topic Actions from last meeting

* #topic Issues reviewing

    Review the issues and talk on any issues we would to higlight.

* #topic Pull Request reviewing

    Review the PRs we consider important.
* #topic open discussion


### 3. Useful information
#### Frequently used meeting commands
THe common used commands are below. To find more, please visit [https://kiwiirc.com/docs/client/commands](https://kiwiirc.com/docs/client/commands) to find more commands. 

### Meeting Archives

Meeting minutes on Feb 25, 2020: https://mailweb.openeuler.org/hyperkitty/list/community@openeuler.org/thread/4LOWQMRP3CYUFIF2E2PT526S3VLA2XX5/

Meeting minutes on Feb 18, 2020: http://meetings.openeuler.org/openeuler-meeting/2020/community/2020-02-18-02.00.html

Meeting minutes on Feb 11, 2020: http://meetings.openeuler.org/openeuler-meeting/2020/community/2020-02-11-02.00.html

Meeting minutes on Fed 4, 2020: http://meetings.openeuler.org/openeuler-meeting/2020/community/2020-02-04-02.00.html

Meeting minutes on Jan 21, 2020: http://meetings.openeuler.org/openeuler-meeting/2020/community/2020-01-21-01.01.html

Meeting minutes on Jan 14, 2020: http://meetings.openeuler.org/openeuler-meeting/2020/community/2020-01-14-02.00.html

Meeting minutes on Jan 7, 2020: http://meetings.openeuler.org/openeuler-meeting/2020/community/2020-01-07-01.59.html

Meeting minutes on Jan. 5, 2020: http://meetings.openeuler.org/openeuler-meeting/2020/community/2020-01-05-13.13.html
