# Technical Committee

The openEuler technical committee is the technical decision-making body of the openEuler community and is responsible for the coordination of technical decisions and technical resources in the community. The main responsibilities of the technical committee are:

- Guidance and decision on community technology route, interface definition, architecture design, build release, etc.

- Coordinate cross-projects cooperation and make decisions on cross-project technical issues in the community

- Formulate project incubation and development processes to support the healthy development of community technology ecology

- Approve new projects to join the community and help community developers incubate new projects

- Adjust existing projects in the community according to the community development blueprint, delete or archive projects that do not meet the community plan

- Accept feedback (needs and questions) from user committees, clarify technical implementation plans, and draw community resources to bring them to the project

- Establish community certification standards and platforms to provide technical support for community certification (OS commercial release certification, hardware compatibility certification, etc.)



# Meeting

- Meeting schedule: to be updated



# Members


- 熊伟[[@myeuler](https://gitee.com/myeuler)]
- cynthia[[@cynthia_xh](https://gitee.com/cynthia_xh)]
- Shinwell[[@Shinwell_Hu](https://gitee.com/Shinwell_Hu)]
- 王勋[[@dream0819](https://gitee.com/dream0819)]
- 郭寒军[[@hanjun-guo](https://gitee.com/hanjun-guo)]
- 谢秀奇[[@xiexiuqi](https://gitee.com/xiexiuqi)]
- zhanghailiang[[@zhanghai_lucky](https://gitee.com/zhanghailiang_lucky)]



# Contact

- [Mail List](tc@openeuler.org)

